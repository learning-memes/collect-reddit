{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Options.Applicative
import Control.Monad.Catch
import Control.Monad.IO.Class
import System.IO
import Control.Monad.Reader
import Data.Semigroup ((<>))
import Data.Aeson
import Data.Bifunctor
import Data.Either
import Data.Maybe
import Data.String.Conv
import Data.Text
import GHC.Generics hiding (from)
import Network.HTTP.Client hiding (Proxy(..))
import Network.HTTP.Client.TLS
import Numeric.Natural
import           Reddit                  (LoginDetails, LoginMethod (..),
                                          Options (..), RedditOptions (..),
                                          RedditT, SubredditName (..),
                                          getPosts', runRedditAnon,
                                          runRedditWith)
import Reddit.Login (login)
import Reddit.Types.Listing (Listing (..), ListingType (..))
import Reddit.Types.Post
import Reddit.Types.Subreddit hiding (title)

import Data.Proxy
import           Data.Aeson
import           Data.Semigroup         ((<>))
import           Data.Text
import           Options.Applicative
import           System.Exit

import Collect
import Collect.Has
import Collect.Meme
import Collect.Source

main :: IO ()
main = do
  options <- execParser redditEnvOpts 
  runCollector (Proxy @RedditSource) (Proxy @Meme) "" =<< options 

data RedditSource
  = RedditSource
  { subredditName :: Text
  , postLimit     :: Natural
  , pullFreq      :: PullFreq
  } deriving (Show, Eq, Generic, ToJSON, FromJSON)

instance Has RedditSource PullFreq where
  get = pullFreq

data RedditCredentials
  = RedditCredentials
  { redditUserName :: Text
  , redditPassword :: Text
  } deriving (Generic, FromJSON)

userNameOpt :: Parser Text
userNameOpt = strOption
  ( long "reddit_username"
  <> metavar "REDDIT_USER_NAME"
  <> help "User name for reddit account"
  )

passwordOpt :: Parser Text
passwordOpt = strOption
  ( long "reddit_password"
  <> metavar "REDDIT_PASSWORD"
  <> help "Password for reddit account"
  )

redditCredentialOpts :: Parser RedditCredentials
redditCredentialOpts
  = RedditCredentials
  <$> userNameOpt
  <*> passwordOpt

data RedditEnv
  = RedditEnv
  { redditTlsManager :: Manager
  , redditUserAgent  :: Text
  , redditLogin      :: LoginDetails
  }

data RedditEnvLoadException
  = RedditLoginFailure

instance Show RedditEnvLoadException where
  show RedditLoginFailure =
    "Could not authenticate with reddit using the credentials provided!"

instance Exception RedditEnvLoadException

loadRedditEnv ::
  ( MonadThrow m
  , MonadIO m
  ) => RedditCredentials -> m RedditEnv
loadRedditEnv (RedditCredentials name pass) = do
  manager <- newTlsManager
  (liftIO . runRedditAnon $ login (toS name) (toS pass)) >>= \case
    Left e -> throwM RedditLoginFailure
    Right details -> pure $ RedditEnv manager "testing" details

redditEnvOpts :: (MonadThrow m, MonadIO m) => ParserInfo (m RedditEnv)
redditEnvOpts = info (fmap loadRedditEnv redditCredentialOpts)
  ( fullDesc
  <> progDesc "Collector of Reddit Memes"
  <> header "Meme Machine")

instance
  ( From Post b
  , MonadReader RedditEnv m
  , MonadThrow m
  , MonadIO m
  )
  => Source RedditEnv m b RedditSource where
  pull source = do
    options <- getOptions
    runRedditWith options (gatherPosts source) >>=
      either
        (throwM . GatherFailure . pack . show)
        (pure . catMaybes . fmap from)

getOptions :: (MonadReader RedditEnv m) => m RedditOptions
getOptions = do
    (RedditEnv tls agent login) <- ask
    pure $ (RedditOptions
            True
            (Just tls)
            (StoredDetails login)
            (Just (toS agent)))

gatherPosts :: (MonadIO m) => RedditSource -> RedditT m [Post]
gatherPosts (RedditSource sub limit _) = do
  let options = Options Nothing (Just (fromIntegral limit))
  contents <$> getPosts' options Hot (Just $ R sub)

instance From Post Meme where
  from p
    = Meme
    <$> pure (permalink p)
    <*> case content p of
          Link t -> Just t
          _      -> Nothing
    <*> (pure . pure) (created p)
    <*> (pure . pure) (title p)
